package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.ICommandRepository;
import com.t1.yd.tm.constant.ArgumentConstant;
import com.t1.yd.tm.constant.CommandConstant;
import com.t1.yd.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(ArgumentConstant.HELP, CommandConstant.HELP, "Show info about program");
    public static final Command ABOUT = new Command(ArgumentConstant.ABOUT, CommandConstant.ABOUT, "Show info about program");
    public static final Command VERSION = new Command(ArgumentConstant.VERSION, CommandConstant.VERSION, "Show program version");
    public static final Command INFO = new Command(ArgumentConstant.INFO, CommandConstant.INFO, "Show system info");
    public static final Command COMMANDS = new Command(ArgumentConstant.COMMANDS, CommandConstant.COMMANDS, "Show available commands");
    public static final Command ARGUMENTS = new Command(ArgumentConstant.ARGUMENTS, CommandConstant.ARGUMENTS, "Show available arguments");
    public static final Command EXIT = new Command(null, CommandConstant.EXIT, "Exit program");

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, ABOUT, VERSION, INFO, COMMANDS, ARGUMENTS, EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }
}
